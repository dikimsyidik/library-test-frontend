import Head from 'next/head'
import Image from 'next/image'
import { Inter } from '@next/font/google'
import styles from '../styles/Home.module.css'
import Book from   './book'
import Navbar from './navbar'
import { useState,useEffect  } from 'react'
import axios from 'axios'

const inter = Inter({ subsets: ['latin'] })



const products = [
  {
    id: 1,
    name: 'Basic Tee',
    href: '#',
    imageSrc: 'https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg',
    imageAlt: "Front of men's Basic Tee in black.",
    price: '$35',
    color: 'Black',
  },
  // More products...
]


export default function Home() {

  return (
    
    <div className="bg-white">
      <Navbar />
      <Book />  

    </div>
  )
}
